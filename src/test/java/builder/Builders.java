package builder;

public class Builders {
  public static void main(String[] args) {
    System.out.println(BuilderInSubClass.
        builder()
        .age(12)
        .manager(true)
        .build());
    System.out.println(BuilderInSubClass.
        builder()
        .age(42)
        .manager(false)
        .build());
  }
}
