package singleton;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MonostateTests {
  @Test
  public void monostateTest() {
    MonostateExample me = new MonostateExample();
    me.setAge(12);
    me.setName("Test");
    MonostateExample me2 = new MonostateExample();
    System.out.println(me);
    System.out.println(me2);

    Assert.assertEquals(me.getAge(), me2.getAge());
    Assert.assertTrue(me.getName() == me2.getName());
    Assert.assertFalse(me == me2);
  }

}
