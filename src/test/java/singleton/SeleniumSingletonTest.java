package singleton;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SeleniumSingletonTest {
  @Test(dataProvider = "data")
  public void getAWebpageTest(final String url) throws InterruptedException {
    DriverSingleton ds = new DriverSingleton();
    ds.getDriver().get(url);
    Assert.assertEquals(ds.getDriver().getCurrentUrl(), url);
  }

  @Test(dataProvider = "data")
  public void multiThreadTest(final String url) {
    ThreadLocalSingleton.getFirefoxDriver().get(url);
    Assert.assertEquals(ThreadLocalSingleton.getFirefoxDriver().getCurrentUrl(), url);
  }

  @DataProvider(name = "data", parallel = true)
  public Object[][] data() {
    return new Object[][]{
        {"https://www.google.com/"},
        {"https://www.abv.bg/"},
        {"https://www.nasa.gov/"},
        {"https://www.wikipedia.org/"}
    };
  }
}
