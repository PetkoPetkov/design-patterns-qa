package template;

public class ManualCar extends Vehicle {
  @Override
  public void startEngine() {
    System.out.println("Key is turned and the engine started");
  }

  @Override
  public void putInGear() {
    System.out.println("Clutch is pressed and gear is put in 1st gear");
  }
}
