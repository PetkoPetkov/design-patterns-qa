package template.zoo;

public class Dog extends Animal {
  @Override
  public void drinkWater() {
    System.out.println("Drink water");
  }

  public void eat() {
    System.out.println("Eat dog food");
  }
}
