package template.zoo;

public abstract class Animal {
  public void wakeUp() {
    System.out.println("Wake up");
  }

  public void eat() {
    System.out.println("Eat");
  }
  public abstract void drinkWater();

  public final void startDay() {
    wakeUp();
    eat();
    drinkWater();
    sendInformationToKeeper();
  }

  private void sendInformationToKeeper() {
    System.out.println("Send information to keeper");
  }
}
