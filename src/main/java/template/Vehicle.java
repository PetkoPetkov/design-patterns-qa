package template;

public abstract class Vehicle {
  public final void start() {
    startEngine();
    putInGear();
    pressGasPedal();
    System.out.println("Vehicle is moving");
  }

  public void pressGasPedal() {
    System.out.println("Pressing the gas pedal");
  }

  public abstract void startEngine();
  public abstract void putInGear();
}
