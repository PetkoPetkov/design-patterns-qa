package template;

public class AutomaticCar extends Vehicle {
  @Override
  public void startEngine() {
    System.out.println("Start button is pressed and the engine started");
  }

  @Override
  public void putInGear() {
    System.out.println("Gear is put in D mode");
  }
}
