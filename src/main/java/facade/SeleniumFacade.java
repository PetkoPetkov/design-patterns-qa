package facade;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SeleniumFacade {
  private WebDriver driver;
  private WebDriverWait wait;

  public SeleniumFacade() {
    System.setProperty("webdriver.chrome.driver",
        System.getProperty("user.dir") + "path_to_chrome_driver");
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    wait = new WebDriverWait(driver, Duration.ofSeconds(10));
  }

  public void openUrl(String url) {
    driver.get(url);
  }

  public void closeBrowser() {
    driver.quit();
  }

  public void clickElement(WebElement element) {
    wait.until(ExpectedConditions.visibilityOf(element));
    wait.until(ExpectedConditions.elementToBeClickable(element));
    element.click();
  }

  public void typeText() {
    System.out.println("Type text");
  }


}
