package prototype;

import lombok.Data;

@Data
public class Sheep extends Animal{
  private int age;
  private int height;
  private String name;

  public Sheep(int age, String name) {
    super();
    this.name = name;
    this.age = age;
  }

  @Override
  public Animal copy() {
    Sheep sheepClone = new Sheep(this.getAge(), this.getName());
    sheepClone.setHeight(this.getHeight());
    return sheepClone;
  }
}
