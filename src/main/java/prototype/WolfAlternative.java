package prototype;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder(toBuilder = true)
public class WolfAlternative {
  private int speed;
  private int age;
  private int height;
  private String name;
  private List<Animal> preys;
  private WolfAlternative partner;

  public static void main(String[] args) {
    WolfAlternative wolf = WolfAlternative.builder().age(12).height(2).build();
    Sheep sheep = new Sheep(2, "Dolly");
    wolf.setPreys(List.of(sheep));
    wolf.setPartner(WolfAlternative.builder().build());

    WolfAlternative wolfClone = wolf.toBuilder().age(2).build();
    System.out.println(wolf == wolfClone);
    System.out.println(wolf);
    System.out.println(wolfClone);
    System.out.println("First prey: " + (wolf.getPreys().get(0) == wolfClone.getPreys().get(0)));
    System.out.println(wolf.getPartner() == wolfClone.getPartner());
    System.out.println(wolf.getPreys().get(0));
    System.out.println(wolfClone.getPreys().get(0));
  }
}
