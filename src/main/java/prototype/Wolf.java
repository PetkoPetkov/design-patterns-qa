package prototype;

import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class Wolf extends Animal{
  private int speed;
  private int age;
  private int height;
  private String name;
  private List<Animal> preys;

  public Wolf(int age, int speed) {
    super();
    this.speed = speed;
    this.age = age;
  }



  @Override
  public Animal copy() {
    Wolf wolfClone = new Wolf(this.getAge(), this.getSpeed());
    wolfClone.setHeight(this.getHeight());
    wolfClone.setPreys(this.getPreys().stream().map(Animal::copy).collect(Collectors.toList()));
    return wolfClone;
  }

  public static void main(String[] args) {

    Wolf wolf = new Wolf(12, 133);
    Sheep sheep = new Sheep(2, "Dolly");
    wolf.setPreys(List.of(sheep));

    Wolf wolfClone = (Wolf)wolf.copy();
    System.out.println(wolf.getPreys().get(0) == wolfClone.getPreys().get(0));
    System.out.println(wolf.getPreys().get(0));
    System.out.println(wolfClone.getPreys().get(0));
  }
}
