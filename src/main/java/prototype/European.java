package prototype;

import lombok.Data;

@Data
public class European extends Person {
  private int age;
  private String name;

  public European(int age, String name) {
    this.age = age;
    this.name = name;
  }

  @Override
  public Person copy() {
    European europeanCopy = new European(this.getAge(), this.getName());
    return europeanCopy;
  }
}
