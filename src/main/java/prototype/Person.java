package prototype;

public abstract class Person {
  public abstract Person copy();
}
