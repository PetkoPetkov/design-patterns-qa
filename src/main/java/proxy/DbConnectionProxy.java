package proxy;

public class DbConnectionProxy implements DbConnection {
  private static DbConnectionImpl dbConnection;
  @Override
  public void query(String query) {
    if (dbConnection == null) {
      dbConnection = new DbConnectionImpl();
    }
    dbConnection.query(query);
  }
}
