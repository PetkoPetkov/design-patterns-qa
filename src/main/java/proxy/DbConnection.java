package proxy;

public interface DbConnection {
  void query(String query);
}
