package proxy;

public class DbConnectionImpl implements DbConnection{
  private String url;
  private String username;
  private String password;
  public DbConnectionImpl() {
    connect();
  }

  @Override
  public void query(String query) {
    System.out.println("Querying...");
  }

  private void connect() {
    System.out.println("Connecting...");
  }
}
