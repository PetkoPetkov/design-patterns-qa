package observer.zoo;

import java.util.ArrayList;
import java.util.List;

public class Lion implements Subject {
  private final List<Observer> observers = new ArrayList<>();
  private boolean isFed;

  public void setFed(boolean isFed) {
    this.isFed = isFed;
    notifyObservers("Lion is " + (isFed ? "fed" : "hungry"));
  }

  @Override
  public void addObserver(Observer observer) {
    observers.add(observer);
  }

  @Override
  public void removeObserver(Observer observer) {
    observers.remove(observer);
  }

  @Override
  public void notifyObservers(String update) {
    observers.forEach(observer -> observer.update(update));
  }
}
