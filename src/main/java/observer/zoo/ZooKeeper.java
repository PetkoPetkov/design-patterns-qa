package observer.zoo;

public class ZooKeeper implements Observer {
  @Override
  public void update(String update) {
    System.out.println("ZooKeeper: " + update);
  }
}
