package observer.zoo;

public interface Observer {
  void update(String update);
}
