package iterator.vehicle;

public interface Container {
  Iterator getIterator();
}
