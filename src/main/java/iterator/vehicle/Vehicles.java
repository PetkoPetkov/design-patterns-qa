package iterator.vehicle;

public class Vehicles implements Container {
  String[] vehicles = {"Car", "Bus", "Bike", "Truck", "Van"};

  @Override
  public Iterator getIterator() {
    return new VehicleIterator();
  }

  public static void main(String[] args) {
    Vehicles vehicles = new Vehicles();
    for (Iterator iterator = vehicles.getIterator(); iterator.hasNext();) {
      String vehicle = (String) iterator.next();
      System.out.println("Vehicle: " + vehicle);
    }
  }

  private class VehicleIterator implements Iterator {

    private int index;

    @Override
    public boolean hasNext() {
      return index < vehicles.length;
    }

    @Override
    public Object next() {
      if (this.hasNext()) {
        return vehicles[index++];
      }
      return null;
    }
  }
}
