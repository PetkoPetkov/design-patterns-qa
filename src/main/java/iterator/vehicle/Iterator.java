package iterator.vehicle;

public interface Iterator {
  boolean hasNext();
  Object next();
}
