package iterator;

public interface Container<E> {
  Iterator<E> getIterator();
}
