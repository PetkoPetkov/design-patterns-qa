package iterator;


public interface Iterator<E> {
  boolean hasNext();
  E next();
  boolean hasPrevious();
  E previous();
}
