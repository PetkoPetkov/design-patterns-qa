package iterator;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class JavaIterator {
    public static void main(String[] args) {
      List<String> list = new ArrayList<>() {
        {
          add("Lion");
          add("Tiger");
          add("Bear");
          add("Elephant");
        }
      };
      Iterator<String> iterator = list.iterator();
      iterator.hasNext();
      System.out.println(iterator.next());
      iterator.remove();
      System.out.println(iterator.next());
      System.out.println(list);
    }
}
