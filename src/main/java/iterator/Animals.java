package iterator;

public class Animals implements Container<String> {
  public String[] names = {"Lion", "Tiger", "Bear", "Elephant"};

  @Override
  public Iterator<String> getIterator() {
    return new AnimalIterator();
  }

  public static void main(String[] args) {
    Animals animals = new Animals();
    Iterator<String> it = animals.getIterator();
    while (it.hasNext()) {
      String name = it.next();
      System.out.println("Name: " + name);
    }
    while (it.hasPrevious()) {
      String name = it.previous();
      System.out.println("Name: " + name);
    }
  }

  private class AnimalIterator implements Iterator<String> {
  int index;
    @Override
    public boolean hasNext() {
      return index < names.length;
    }

    @Override
    public String next() {
      if (this.hasNext()) {
        return names[index++];
      }
      return null;
    }

    @Override
    public boolean hasPrevious() {
      return index != 0;
    }

    @Override
    public String previous() {
      if (this.hasPrevious()) {
        return names[index--];
      }
      return null;
    }
  }
}
