package state;

public interface VehicleState {
  String getStateName();
}
