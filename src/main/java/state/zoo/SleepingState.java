package state.zoo;

public class SleepingState implements AnimalState {
  @Override
  public String getStateName() {
    return "sleeping";
  }
}
