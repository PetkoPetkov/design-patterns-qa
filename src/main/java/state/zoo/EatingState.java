package state.zoo;

public class EatingState implements AnimalState {
  @Override
  public String getStateName() {
    return "eating";
  }
}
