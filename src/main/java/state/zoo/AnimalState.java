package state.zoo;

public interface AnimalState {
  String getStateName();
}
