package state.zoo;

public class Lion {
  private AnimalState state;

  public void showState() {
    System.out.println("The lion is in " + state.getStateName() + " state");
  }

  public void setState(AnimalState state) {
    if (state == null) {
      this.state = new SleepingState();
    } else {
      this.state = state;
    }
  }

  public Lion(AnimalState state) {
    setState(state);
  }

  public static void main(String[] args) {
    Lion lion = new Lion(new SleepingState());
    lion.showState();
    lion.setState(new EatingState());
    lion.showState();
    lion.setState(null);
    lion.showState();
  }
}
