package state;

public class ParkedState implements VehicleState {
  @Override
  public String getStateName() {
    return "Parked";
  }
}
