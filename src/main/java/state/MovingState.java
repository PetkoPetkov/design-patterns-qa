package state;

public class MovingState implements VehicleState {
  @Override
  public String getStateName() {
    return "Moving";
  }
}
