package state;

import lombok.Data;

@Data
public class Vehicle {
  private VehicleState state;

  public Vehicle(VehicleState state) {
    this.state = state;
  }

  public void setState(VehicleState state) {
    if (state == null) {
      throw new IllegalArgumentException("State cannot be null");
    }
    this.state = state;
  }

  public void showState() {
    System.out.println("The vehicle is in " + state.getStateName() + " state.");
  }
}
