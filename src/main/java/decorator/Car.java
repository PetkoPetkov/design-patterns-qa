package decorator;

import lombok.Data;

@Data
public class Car implements Vehicle{
  @Override
  public void runsOn() {
    System.out.println("Runs on gas");
    addAdditive();
  }



  private void addAdditive() {
    System.out.println("Additive added to " + this.toString());
  }
}
