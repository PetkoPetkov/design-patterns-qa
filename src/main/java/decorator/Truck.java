package decorator;

public class Truck implements Vehicle {
  @Override
  public void runsOn() {
    System.out.println("Runs on petrol");
  }
}
