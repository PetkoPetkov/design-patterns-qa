package decorator.family;

public interface Family {
  void giveInformation();
}
