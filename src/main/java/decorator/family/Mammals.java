package decorator.family;

public class Mammals implements Family{
  @Override
  public void giveInformation() {
    System.out.println("Mammals give birth to their offspring!");
  }
}
