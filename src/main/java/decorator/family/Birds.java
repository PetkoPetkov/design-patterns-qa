package decorator.family;

public class Birds implements Family {

  @Override
  public void giveInformation() {
    System.out.println("Birds lay eggs!");
  }
}
