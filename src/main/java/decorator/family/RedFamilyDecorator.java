package decorator.family;

public class RedFamilyDecorator extends FamilyDecorator{
  public RedFamilyDecorator(Family family) {
    super(family);
  }

  @Override
  public void giveInformation() {
    family.giveInformation();
    animalColor();
  }

  private void animalColor() {
    System.out.println("The animal is red");
  }
}
