package decorator.family;

public abstract class FamilyDecorator implements Family {
  protected Family family;

  public FamilyDecorator(Family family) {
    this.family = family;
  }
}
