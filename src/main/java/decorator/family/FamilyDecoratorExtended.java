package decorator.family;

public class FamilyDecoratorExtended extends FamilyDecorator {
  public FamilyDecoratorExtended(Family family) {
    super(family);
  }

  public void printAllAnimals() {
    System.out.println("List all animals");
  }

  @Override
  public void giveInformation() {
    System.out.println("All animals are awesome");
    family.giveInformation();
    printAllAnimals();
  }

  public static void main(String[] args) {
    new FamilyDecoratorExtended(new RedFamilyDecorator(new FamilyDecoratorExtended(new Birds()))).giveInformation();
  }
}
