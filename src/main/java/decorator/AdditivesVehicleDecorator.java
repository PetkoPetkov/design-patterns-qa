package decorator;

public abstract class AdditivesVehicleDecorator implements Vehicle {
  protected Vehicle vehicle;

  public AdditivesVehicleDecorator(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  @Override
  public void runsOn() {
    vehicle.runsOn();
  }
}
