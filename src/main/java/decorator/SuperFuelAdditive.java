package decorator;

public class SuperFuelAdditive extends AdditivesVehicleDecorator {
  public SuperFuelAdditive(Vehicle vehicle) {
    super(vehicle);
  }

  @Override
  public void runsOn() {
    vehicle.runsOn();
    addAdditive();
  }

  private void addAdditive() {
    System.out.println("Additive added to " + vehicle.toString());
  }

  public static void main(String[] args) {
    new SuperFuelAdditive(new SuperFuelAdditive(new Car())).runsOn();
  }
}
