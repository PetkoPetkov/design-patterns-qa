package decorator;

public interface Vehicle {
  void runsOn();
}
