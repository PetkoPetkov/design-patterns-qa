package mediator;

public class GasPedal {
  public void excelerate() {
    System.out.println("Gas pedal is pressed");
  }

  public void decelerate() {
    System.out.println("Gas pedal is released");
  }
}
