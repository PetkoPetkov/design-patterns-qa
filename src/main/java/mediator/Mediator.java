package mediator;

public class Mediator {
  private final Car car;
  private final GasPedal gasPedal;

  public Mediator(Car car, GasPedal gasPedal) {
    this.car = car;
    this.gasPedal = gasPedal;
  }

  public void pressPedal() {
    if (car.isMoving()) {
      car.setMoving(false);
      gasPedal.decelerate();
    } else {
      car.setMoving(true);
      gasPedal.excelerate();
    }
    System.out.println(car.getName() + (car.isMoving() ? " starts" : " stops") + " moving");
  }
}
