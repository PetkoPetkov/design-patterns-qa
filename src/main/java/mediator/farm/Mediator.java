package mediator.farm;


public class Mediator {
  private final Farmer farmer;
  private final Animal animal;

  public Mediator(Farmer farmer, Animal animal) {
    this.farmer = farmer;
    this.animal = animal;
  }

  public void takeCare() {
    if (animal.isHungry()) {
      farmer.feed();
      animal.setHungry(false);
    } else {
      farmer.groom();
      animal.setHungry(true);
    }
  }
}
