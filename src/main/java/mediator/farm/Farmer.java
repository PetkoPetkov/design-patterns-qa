package mediator.farm;

public class Farmer {

  public void feed() {
    System.out.println("Feeding the animal");
  }

  public void groom() {
    System.out.println("Grooming the animal");
  }
}
