package mediator.farm;

public class Animal {
  private boolean isHungry;
  private final Mediator mediator;

  public Animal(Mediator mediator) {
    this.mediator = mediator;
  }

  public void eat() {
    if (isHungry) {
      mediator.takeCare();
    }
  }

  public void gettingGroomed() {
    if (!isHungry) {
      mediator.takeCare();
    }
  }

  public boolean isHungry() {
    return isHungry;
  }

  public void setHungry(boolean isHungry) {
    this.isHungry = isHungry;
  }
}
