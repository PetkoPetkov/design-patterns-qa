package mediator;

import lombok.Data;

@Data
public class Car {
    private Mediator mediator;
    private boolean isMoving;
    private String name;

    public Car(Mediator mediator) {
        this.mediator = mediator;
        isMoving = false;
    }

    public void start() {
        if (!isMoving) {
            mediator.pressPedal();
        }
    }

    public void stop() {
        if (isMoving) {
            mediator.pressPedal();
        }
    }
}
