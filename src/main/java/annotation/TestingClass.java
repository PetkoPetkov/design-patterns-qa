package annotation;

import lombok.Data;

@Data
public class TestingClass {
  @CheckValue("Hello World")
  public String test;

  public static void main(String[] args) {
    TestingClass testingClass = new TestingClass();
    testingClass.setTest("Hello World");
    System.out.println(testingClass.getTest());
  }
}
