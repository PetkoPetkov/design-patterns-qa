package annotation;

import java.lang.reflect.Field;

public class CheckValueProcessor {

  public static boolean validate(Object obj) throws IllegalAccessException {
    Class<?> objClass = obj.getClass();
    for (Field field : objClass.getDeclaredFields()) {
      if (field.isAnnotationPresent(CheckValue.class)) {
        field.setAccessible(true);
        CheckValue checkValue = field.getAnnotation(CheckValue.class);
        Object fieldValue = field.get(obj);
        if (fieldValue == null || !fieldValue.toString().equals(checkValue.value())) {
          return false;
        }
      }
    }
    System.out.println("All fields are valid");
    return true;
  }
}