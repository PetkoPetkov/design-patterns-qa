package adapter;

public class RectangleImpl implements Rectangle {
  private int width;
  private int height;

  public RectangleImpl(int width, int height) {
    this.width = width;
    this.height = height;
  }

  @Override
  public double calculateArea() {
    return width * height;
  }
}
