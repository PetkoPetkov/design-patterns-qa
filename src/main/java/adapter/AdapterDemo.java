package adapter;

public class AdapterDemo {
  static double doSomeRectangleAction(Rectangle rectangle) {
    System.out.println("Preliminary action");
    double result = rectangle.calculateArea();
    System.out.println("Afteraction");
    return result;
  }

  public static void main(String[] args) {
    Square square = new Square(2);

    double result = doSomeRectangleAction(new RectangleImpl(1,1));
    double result1 = doSomeRectangleAction(new SquareToRectangleAdapter(square));



    System.out.println(result1);
    System.out.println(result);
  }
}
