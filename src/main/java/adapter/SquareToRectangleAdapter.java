package adapter;

public class SquareToRectangleAdapter implements Rectangle {
  private final Square square;

  public SquareToRectangleAdapter(Square square) {
    this.square = square;
  }

  @Override
  public double calculateArea() {
    System.out.println("Calcualting Rectangle area:");
    return square.getSide() * square.getSide();
  }
}
