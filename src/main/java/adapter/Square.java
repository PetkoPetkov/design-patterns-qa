package adapter;

import lombok.Getter;

@Getter
public class Square {
  private int side;

  public Square(int side) {
    this.side = side;
  }
  public double calculateArea() {
    return Math.pow(getSide(), 2);
  }
}
