package adapter.mobileapp;

public class BrowserToMobileAppAdapter implements BrowserApp {
  private MobileApp mobileApp;

  public BrowserToMobileAppAdapter(MobileApp mobileApp) {
    this.mobileApp = mobileApp;
  }
  @Override
  public void displayWelcomeScreen() {
    mobileApp.displayWelcomeScreen();
  }

}
