package adapter.mobileapp;

public class BrowserAppImpl implements BrowserApp {
  @Override
  public void displayWelcomeScreen() {
    System.out.println("Browser app: Welcome!");
  }
}
