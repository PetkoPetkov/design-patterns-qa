package adapter;

public interface Rectangle {
  double calculateArea();
}
