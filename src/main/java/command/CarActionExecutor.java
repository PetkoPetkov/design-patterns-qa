package command;

import java.util.ArrayList;
import java.util.List;

public class CarActionExecutor {
  private final List<CarAction> carActions = new ArrayList<>();

  public String executeCarActions(CarAction carAction) {
    carActions.add(carAction);
    return carAction.execute();
  }
}
