package command;

public class StartCarAction implements CarAction {
  private Car car;

  public StartCarAction(Car car) {
    this.car = car;
  }

  public String execute() {
    car.start();
    return "Starting";
  }
}
