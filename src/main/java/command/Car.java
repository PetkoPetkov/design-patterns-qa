package command;

public class Car {
  private String name;
  public Car(String name) {
    this.name = name;
  }

  public String start() {
    return name + " is starting";
  }

  public String excelerate() {
    return name + " is excelerating";
  }
}
