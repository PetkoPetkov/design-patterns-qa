package command;

public interface CarAction {
  String execute();
}
