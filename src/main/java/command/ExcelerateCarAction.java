package command;

public class ExcelerateCarAction implements CarAction {
  private Car car;

  public ExcelerateCarAction(Car car) {
    this.car = car;
  }

  public String execute() {
    car.excelerate();
    return "Excelerating";
  }
}
