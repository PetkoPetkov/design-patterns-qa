package command;

public class Client {
  public static void main(String[] args) {
    Car car = new Car("Volvo");
    StartCarAction startCarAction = new StartCarAction(car);
    CarActionExecutor carActionExecutor = new CarActionExecutor();
    carActionExecutor.executeCarActions(startCarAction);
    carActionExecutor.executeCarActions(new ExcelerateCarAction(car));
  }
}
