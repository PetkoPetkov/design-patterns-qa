package command.zoo;

public class AnimalRun implements AnimalAction {
  public void execute() {
    System.out.println("Animal is running");
  }
}
