package command.zoo;

public class AnimalFeed implements AnimalAction {
  @Override
  public void execute() {
    System.out.println("Animal is eating");
  }
}
