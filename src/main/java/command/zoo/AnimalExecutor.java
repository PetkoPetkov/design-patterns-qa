package command.zoo;

public class AnimalExecutor {
  public void execute(AnimalAction action) {
    action.execute();
  }

  public static void main(String[] args) {
    AnimalExecutor executor = new AnimalExecutor();
    executor.execute(new AnimalFeed());
    executor.execute(new AnimalRun());
  }
}
