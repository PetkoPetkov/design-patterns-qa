package command.zoo;

public interface AnimalAction {
  void execute();
}
