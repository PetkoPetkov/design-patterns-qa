package builder;

public class GroupingMehodsForBuiding {
  private String name;
  private int age;
  private String title;
  private String position;
  private boolean isManager;
  private int salary;
  private int yearsExperience;


  public static void main(String[] args) {
    GroupingMehodsForBuiding gms = new GroupingMehodsForBuiding();
    gms.setNameAge("Test", 123);

    System.out.println(gms.age);
  }

  public void setNameAge(final String name, final int age) {
    this.name = name;
    this.age = age;
  }

  public void setTitlePosition(final String title, final String position) {
    this.title = title;
    this.position = position;
  }

  public void setSalaryExperience(final int salary, final int yearsExperience) {
    this.salary = salary;
    this.yearsExperience = yearsExperience;
  }

}
