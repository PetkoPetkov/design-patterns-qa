package builder;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class LombokSavesTheDay {
  private String name;
  private int age;
  private String title;
  private String position;
  private boolean isManager;
  private int salary;
  private Integer yearsExperience;


  public static void main(String[] args) {
    LombokSavesTheDay ld = LombokSavesTheDay.builder()
        .name("test")
        .build();
    ld.setAge(123);
    System.out.println(ld);
  }
}
