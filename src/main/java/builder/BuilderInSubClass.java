package builder;

public class BuilderInSubClass {
  private String name;
  private int age;
  private String title;
  private String position;
  private boolean isManager;
  private int salary;
  private int yearsExperience;

  private BuilderInSubClass(final BuilderInSubClassBuilder builder) {
    this.name = builder.name;
    this.age = builder.age;
    this.title = builder.title;
    this.position = builder.position;
    this.isManager = builder.isManager;
    this.salary = builder.salary;
    this.yearsExperience = builder.yearsExperience;
  }
  public static BuilderInSubClassBuilder builder() {
    return new BuilderInSubClassBuilder();
  }

  @Override
  public String toString() {
    return String.join(",", "age: " + age, "position: " + position, "Is manager: " + isManager);
  }

  public static class BuilderInSubClassBuilder {
    private String name;
    private int age;
    private String title;
    private String position;
    private boolean isManager;
    private int salary;
    private int yearsExperience;


    private BuilderInSubClassBuilder() {}

    public BuilderInSubClassBuilder name(String name) {
      this.name = name;
      return this;
    }

    public BuilderInSubClassBuilder age(int age) {
      this.age = age;
      return this;
    }

    public BuilderInSubClassBuilder title(String title) {
      this.title = title;
      return this;
    }

    public BuilderInSubClassBuilder position(String position) {
      this.position = position;
      return this;
    }

    public BuilderInSubClassBuilder manager(boolean manager) {
      isManager = manager;
      return this;
    }

    public BuilderInSubClassBuilder salary(int salary) {
      this.salary = salary;
      return this;
    }

    public BuilderInSubClassBuilder yearsExperience(int yearsExperience) {
      this.yearsExperience = yearsExperience;
      return this;
    }

    public BuilderInSubClass build() {
      return new BuilderInSubClass(this);
    }
  }

}
