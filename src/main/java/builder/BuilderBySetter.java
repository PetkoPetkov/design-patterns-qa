package builder;

public class BuilderBySetter {
  private String name;
  private int age;
  private String title;
  private String position;
  private boolean isManager;
  private int salary;
  private int yearsExperience;

  public static void main(String[] args) {
    BuilderBySetter  bs = new BuilderBySetter()
        .setName("Test")
        .setTitle("Mr");

    System.out.println(bs.name);
  }

  public BuilderBySetter(String name, int age, String title, String position, boolean isManager, int salary,
      int yearsExperience) {
    this.name = name;
    this.age = age;
    this.title = title;
    this.position = position;
    this.isManager = isManager;
    this.salary = salary;
    this.yearsExperience = yearsExperience;
  }

  BuilderBySetter() {}

  public BuilderBySetter setName(String name) {
    this.name = name;
    return this;
  }

  public BuilderBySetter setAge(int age) {
    this.age = age;
    return this;
  }

  public BuilderBySetter setTitle(String title) {
    this.title = title;
    return this;
  }

  public BuilderBySetter setPosition(String position) {
    this.position = position;
    return this;
  }

  public BuilderBySetter setManager(boolean manager) {
    isManager = manager;
    return this;
  }

  public BuilderBySetter setSalary(int salary) {
    this.salary = salary;
    return this;
  }

  public BuilderBySetter setYearsExperience(int yearsExperience) {
    this.yearsExperience = yearsExperience;
    return this;
  }

  @Override
  public String toString() {
    return String.join(",", "age: " + age, "position: " + position, "Is manager: " + isManager);
  }
}
