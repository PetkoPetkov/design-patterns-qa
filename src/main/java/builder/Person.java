package builder;

public class Person {
  private String name;
  private int age;

  private Person(PersonBuilder builder) {
    this.name = name;
    this.age = age;
  }

  public static PersonBuilder builder() {
    return new PersonBuilder();
  }

  public static void main(String[] args) {
    Person.builder()
        .setAge(12)
        .setName("test")
        .build();
  }
  public static class PersonBuilder {
    private String name;
    private int age;

    private PersonBuilder() {

    }

    public PersonBuilder setName(String name) {
      this.name = name;
      return this;
    }

    public PersonBuilder setAge(int age) {
      this.age = age;
      return this;
    }

    public Person build() {
      return new Person(this);
    }
  }
}
