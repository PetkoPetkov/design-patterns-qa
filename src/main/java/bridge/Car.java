package bridge;
public class Car extends Vehicle {

  public Car(Fuel fuel) {
    super(fuel);
  }
  @Override
  public String move() {
    return "Moving..." + fuel.powerUp();
  }
}
