package bridge;

public abstract class Vehicle {
  protected Fuel fuel;

  public Vehicle(Fuel fuel) {
    this.fuel = fuel;
  }
  abstract public String move();
}
