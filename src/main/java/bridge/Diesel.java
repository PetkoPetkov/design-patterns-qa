package bridge;

public class Diesel implements Fuel {
  @Override
  public String powerUp() {
    return "powered by Diesel";
  }
}
