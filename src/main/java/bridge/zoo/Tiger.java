package bridge.zoo;

public class Tiger extends Animal {
  public Tiger(Family family) {
    super(family);
  }

  @Override
  public void giveFullInformation() {
    family.giveInformation();
    System.out.println("Tiger info");
  }

  public static void main(String[] args) {
    Tiger tiger = new Tiger(new Felidae());
    tiger.giveFullInformation();
  }
}
