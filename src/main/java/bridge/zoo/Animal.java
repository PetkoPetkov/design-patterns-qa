package bridge.zoo;

public abstract class Animal {
  protected Family family;

  public Animal (Family family) {
    this.family = family;
  }

  public abstract void giveFullInformation();
}
