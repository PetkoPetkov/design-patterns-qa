package bridge.zoo;

public interface Family {
  void giveInformation();
}
