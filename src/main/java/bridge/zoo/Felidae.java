package bridge.zoo;

public class Felidae implements Family {
  @Override
  public void giveInformation() {
    System.out.println("Some info");
  }
}
