package bridge;

public interface Fuel {
  String powerUp();
}
