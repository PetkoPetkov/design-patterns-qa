package bridge;

public class Petrol implements Fuel {
  @Override
  public String powerUp() {
    return "Moving with the power of petrol!";
  }

}
