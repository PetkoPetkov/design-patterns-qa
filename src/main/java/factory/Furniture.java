package factory;

public class Furniture {
  private String material;
  private String color;
  private String size;

  private Furniture(String material, String color) {
    this.material = material;
    this.color = color;
  }

  public static Furniture createBed(String size) {
    String bedMaterial = "wood";
    String bedColor = "blue";
    Furniture f = new Furniture(bedMaterial, bedColor);
    f.size = size;
    return f;
  }
}
