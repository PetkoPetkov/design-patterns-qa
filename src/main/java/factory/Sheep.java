package factory;

public class Sheep implements Animal {

  @Override
  public void run() {
    System.out.println("A sheep is running");
  }
}
