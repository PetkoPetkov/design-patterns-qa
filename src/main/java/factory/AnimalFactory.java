package factory;

public class AnimalFactory {
  public static Animal createAnimal(AnimalTypes animal) {
    switch (animal) {
      case SHEEP:
        return new Sheep();
      case WOLF:
        return new Wolf();
      default:
        return null;
    }
  }

  public enum AnimalTypes {
    SHEEP, WOLF
  }

  public static void main(String[] args) {
    AnimalFactory.createAnimal(AnimalTypes.SHEEP).run();
  }
}
