package factory;

public class Wolf implements Animal {

  @Override
  public void run() {
    System.out.println("A wolf is running");
  }

}
