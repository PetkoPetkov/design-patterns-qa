package factory;

import lombok.Data;

import java.util.List;

@Data
public class Figure {
  private List<Integer> dimensions;
  private double area;

  private Figure(Integer... dimentions) {
    this.dimensions = List.of(dimentions);
  }

  public static Figure createSquare(Integer side) {
    Figure fa = new Figure(side);
    fa.area = Math.pow(side, 2);
    return fa;
  }

  public static Figure createCircle(Integer radius) {
    Figure fa = new Figure(radius);
    fa.area = Math.PI * Math.pow(radius, 2);
    return fa;
  }

  public static void main(String[] args) {
    Figure square = Figure.createSquare(3);
    System.out.println(square.getArea());

    Figure circle = Figure.createCircle(2);
    System.out.println(circle.getArea());
  }
}
