package memento;

import lombok.Data;

@Data
public class UserMemento {
  private String firstName;
  private String lastName;
  private String email;
  private int age;
  public UserMemento(String firstName, String lastName, String email, int age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
  }

  // Additionally we can override the toString() method to print the state of the UserMemento object, this in our
  // case is handled by the @Data annotation from Lombok.
}
