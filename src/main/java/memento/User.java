package memento;

import lombok.Data;

@Data
public class User {
  private String firstName;
  private String lastName;
  private String email;
  private int age;

  public UserMemento createMemento() {
    return new UserMemento(getFirstName(), getLastName(), getEmail(), getAge());
  }

  public void revert(UserMemento userMemento) {
    setFirstName(userMemento.getFirstName());
    setLastName(userMemento.getLastName());
    setEmail(userMemento.getEmail());
    setAge(userMemento.getAge());
  }
}
