package memento;

import java.util.ArrayList;
import java.util.List;

public class UserHistory {
  private final List<UserMemento> userHistory;

  public UserHistory() {
    this.userHistory = new ArrayList<>();
  }

  public void addMementoToHistory(UserMemento userMemento) {
    userHistory.add(userMemento);
  }

  public UserMemento getMemento(int index) {
    return userHistory.get(index);
  }
}
