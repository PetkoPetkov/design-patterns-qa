package memento.zoo;

public class Demo {
  public static void main(String[] args) {
    Animal animal = new Animal();
    History history = new History();
    animal.setColor("Blue");
    animal.setNumberOfLegs(2);
    history.addMemento("blue", animal.createMemento());
    System.out.println(animal);

    animal.setColor("White");
    animal.setNumberOfLegs(4);
    history.addMemento("white", animal.createMemento());
    System.out.println(animal);

    animal.setColor("White");
    animal.setNumberOfLegs(4);
    history.addMemento("blue", animal.createMemento());
    System.out.println(animal);

    animal.revert(history.getMemento("blue"));
    System.out.println(animal);
  }

}
