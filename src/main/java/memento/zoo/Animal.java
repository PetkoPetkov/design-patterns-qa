package memento.zoo;

import lombok.Data;

@Data
public class Animal {
  private int numberOfLegs;
  private String color;

  public Memento createMemento() {
    return new Memento(numberOfLegs, color);
  }

  public void revert(Memento memento) {
    this.numberOfLegs = memento.getNumberOfLegs();
    this.color = memento.getColor();
  }
}
