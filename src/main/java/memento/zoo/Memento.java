package memento.zoo;

import lombok.Getter;

@Getter
public class Memento {
  private int numberOfLegs;
  private String color;
  public Memento(int numberOfLegs, String color) {
    this.color = color;
    this.numberOfLegs = numberOfLegs;
  }
}
