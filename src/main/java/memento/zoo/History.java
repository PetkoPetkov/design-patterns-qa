package memento.zoo;

import java.util.HashMap;
import java.util.Map;

public class History {
  Map<String, Memento> mementos;

  public History() {
    mementos = new HashMap<>();
  }

  public void addMemento(String mementoKey, Memento memento) {
    mementos.put(mementoKey, memento);
  }

  public Memento getMemento(String mementoKey) {
    return mementos.get(mementoKey);
  }
}
