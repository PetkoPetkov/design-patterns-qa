package memento;

public class Demo {
  public static void main(String[] args) {
    User user = new User();
    UserHistory userHistory = new UserHistory();
    user.setFirstName("John");
    user.setLastName("Doe");
    user.setEmail("test@mail.bg");
    user.setAge(25);
    userHistory.addMementoToHistory(user.createMemento());
    System.out.println(user);

    user.setFirstName("Jane");
    user.setLastName("Done");
    user.setEmail("jane@asd.vf");
    user.setAge(30);
    userHistory.addMementoToHistory(user.createMemento());
    System.out.println(user);

    user.revert(userHistory.getMemento(0));
    System.out.println(user);

    user.revert(userHistory.getMemento(1));
    System.out.println(user);
  }
}
