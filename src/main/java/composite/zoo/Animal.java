package composite.zoo;

public interface Animal {
  void feed();
}
