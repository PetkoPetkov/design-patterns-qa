package composite.zoo;

public class Sheep implements Animal {
  @Override
  public void feed() {
    System.out.println("Feeding the sheep");
  }
}
