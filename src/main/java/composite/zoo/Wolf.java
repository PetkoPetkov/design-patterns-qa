package composite.zoo;

public class Wolf implements Animal{
  @Override
  public void feed() {
    System.out.println("Feeding the wolves");
  }
}
