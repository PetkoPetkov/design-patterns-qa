package composite.zoo;

import java.util.ArrayList;
import java.util.List;

public class Zoo implements Animal{
  private final List<Animal> animals;

  public Zoo() {
    animals = new ArrayList<>();
  }

  @Override
  public void feed() {
    for (Animal animal : animals) {
      animal.feed();
    }
  }

  public void addAnimal(Animal animal) {
    animals.add(animal);
  }

  public void removeAnimal(Animal animal) {
    animals.remove(animal);
  }
}
