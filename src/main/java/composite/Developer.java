package composite;

public class Developer implements Person {
  @Override
  public void doesWork() {
    System.out.println("Develops all the features in the world");
  }
}
