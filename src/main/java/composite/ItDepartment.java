package composite;

import java.util.ArrayList;
import java.util.List;

public class ItDepartment implements Person {

  private final List<Person> people;

  public ItDepartment() {
    this.people = new ArrayList<>();
  }

  @Override
  public void doesWork() {
    people.forEach(Person::doesWork);
  }

  public void addPerson(Person person) {
    people.add(person);
  }

  public void removePerson(Person person) {
    people.remove(person);
  }
}
