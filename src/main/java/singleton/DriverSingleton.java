package singleton;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Objects;

public class DriverSingleton {
  private static WebDriver driver;

  public WebDriver getDriver() {
    if (Objects.isNull(driver)) {
      System.setProperty("webdriver.gecko.driver",
          System.getProperty("user.dir") + "\\src\\main\\resources\\geckodriver.exe");
      driver = new FirefoxDriver();
      driver.manage().window().maximize();
    }
    return driver;
  }
}
