package singleton;

import org.openqa.selenium.firefox.FirefoxDriver;

public class ThreadLocalSingleton {
  private static final ThreadLocal<FirefoxDriver> firefoxDriver = ThreadLocal.withInitial(FirefoxDriver::new);

  public static FirefoxDriver getFirefoxDriver() {
    return firefoxDriver.get();
  }
}
