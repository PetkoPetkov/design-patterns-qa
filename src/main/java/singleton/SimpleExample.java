package singleton;

import java.util.Objects;
import java.util.Random;

public class SimpleExample {
  private static String singleString;

  public String getSingleString() {
    if (Objects.isNull(singleString)) {
      singleString = String.valueOf(new Random().nextInt());
    }
    return singleString;
  }

}
