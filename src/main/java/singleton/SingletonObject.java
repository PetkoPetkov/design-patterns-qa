package singleton;

import java.util.Objects;

public class SingletonObject {
  private static SingletonObject instance;

  private SingletonObject() {}

  public static SingletonObject getInstance() {
    if (Objects.nonNull(instance)) {
      instance = new SingletonObject();
    }
    return instance;
  }
}
