package singleton;

public class MonostateExample {
  private static String name;
  private static int age;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    MonostateExample.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    MonostateExample.age = age;
  }
}
