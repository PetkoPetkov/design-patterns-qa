package visitor;

public class VisitorImpl implements Visitor {
  @Override
  public String visit(Car car) {
    return "Car";
  }

  @Override
  public String visit(Truck truck) {
    return "Truck";
  }
}
