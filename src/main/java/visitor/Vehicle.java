package visitor;

public interface Vehicle {
    String accept(Visitor visitor);
}
