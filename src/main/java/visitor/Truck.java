package visitor;

public class Truck implements Vehicle {
  @Override
  public String accept(Visitor visitor) {
    return visitor.visit(this);
  }
}
