package visitor;

public class Car implements Vehicle {
  @Override
  public String accept(Visitor visitor) {
    return visitor.visit(this);
  }
}
