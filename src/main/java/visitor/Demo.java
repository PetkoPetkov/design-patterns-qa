package visitor;

public class Demo {
  public static void main(String[] args) {
    Visitor visitor = new VisitorImpl();
    Vehicle car = new Car();
    Vehicle truck = new Truck();
    System.out.println(car.accept(visitor));
    System.out.println(truck.accept(visitor));
  }
}

