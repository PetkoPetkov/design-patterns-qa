package visitor;

public interface Visitor {
    String visit(Car car);
    String visit(Truck truck);
}
