package visitor.zoo;

public interface Animal {
  void accept(Visitor visitor);
}
