package visitor.zoo;

public class RunningVisitor implements Visitor {
  @Override
  public void visit(Lion lion) {
    System.out.println("Lion is running");
  }

  @Override
  public void visit(Bear bear) {
    System.out.println("Bear is running");
  }
}
