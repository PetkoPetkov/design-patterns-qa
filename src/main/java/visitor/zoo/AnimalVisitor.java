package visitor.zoo;

public class AnimalVisitor implements Visitor {
  @Override
  public void visit(Lion lion) {
    System.out.println("Visitor visiting Lion");
  }

  @Override
  public void visit(Bear bear) {
    System.out.println("Visitor visiting Bear");
  }
}
