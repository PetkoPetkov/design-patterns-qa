package visitor.zoo;

public class Demo {
  public static void main(String[] args) {
    Visitor visitor = new AnimalVisitor();
    Visitor visitor2 = new RunningVisitor();
    Animal lion = new Lion();
    Animal bear = new Bear();
    lion.accept(visitor);
    bear.accept(visitor);

    lion.accept(visitor2);
  }
}
