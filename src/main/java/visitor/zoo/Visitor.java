package visitor.zoo;

public interface Visitor {
  void visit(Lion lion);
  void visit(Bear bear);
}
